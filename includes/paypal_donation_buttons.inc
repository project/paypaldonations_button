<?php

/**
 * @file
 * Donate button node type field add and delete.
 */

// Machine name for our custom node.
define('PAYPAL_DONATION_NODE_NAME', "paypal_donation_buttons");

// Field group name.
define('GROUP_DONATION_BUTTON_DETAILS', 'group_donation_button_details');
define('GROUP_CUSTOMIZE_DONATION_BUTTON', 'group_customize_donation_button');
define('GROUP_TRACK_INVENTORY_PROFIT', 'group_track_inventory_profit');
define('GROUP_ADVANCED_FEATURES', 'group_advanced_features');
define('PARENT_GROUP', 'group_main');

// Paypal general function callback.
module_load_include("inc", "paypal_button_manager",
"includes/paypal_functions");

/**
 * Filed installed.
 */
function _paypal_button_list_installed_fields() {
  $label_title = get_t();

  return array(
    'company_list' => array(
      'field_name' => 'company_list',
      'type' => 'node_reference',
      'label' => $label_title('Finish Url'),
      'settings' => array(
        'referenceable_types' => array(
          'paypal_donation_companay' => 'paypal_donation_companay',
        ),
        'view' => array(
          'view_name' => '',
          'display_name' => '',
          'args' => array(),
        ),
      ),
    ),
    'organization_name_service' => array(
      'field_name' => 'organization_name_service',
      'label' => $label_title('Organization name/service'),
      'type' => 'text',
    ),
    'paypal_donation_id' => array(
      'field_name' => 'paypal_donation_id',
      'label' => $label_title('Donation ID'),
      'type' => 'text',
    ),
    'customize_donation_button_type' => array(
      'field_name' => 'customize_donation_button_type',
      'type' => 'list_text',
      'label' => $label_title('Customize button'),
      'cardinality' => '1',
      'settings' => array(
        'allowed_values' => array(
          'paypal_button' => 'PayPal button',
          'use_your_own_button_image' => 'Use your own button image',
        ),
        'allowed_values_function' => '',
      ),
    ),
    'customize_donation_button' => array(
      'field_name' => 'customize_donation_button',
      'type' => 'list_text',
      'label' => $label_title('PayPal button'),
      'cardinality' => '1',
      'settings' => array(
        'allowed_values' => array(
          'use_smaller_button' => 'Use smaller button',
          'display_credit_card_logos' => 'Display credit card logos',
        ),
        'allowed_values_function' => '',
      ),
    ),
    'country_and_language_button' => array(
      'field_name' => 'country_and_language_button',
      'type' => 'list_text',
      'label' => $label_title('Country and language for button'),
      'settings' => array(
        'allowed_values' => get_paypal_button_languages(),
        'allowed_values_function' => '',
      ),
    ),

    'select_currency' => array(
      'field_name' => 'select_currency',
      'type' => 'list_text',
      'label' => $label_title('Currency'),
      'cardinality' => '1',
      'settings' => array(
        'allowed_values' => get_paypal_button_currency(),
        'allowed_values_function' => '',
      ),
    ),
    'select_contribution_amount' => array(
      'field_name' => 'select_contribution_amount',
      'type' => 'list_text',
      'label' => $label_title('Contribution amount'),
      'cardinality' => '1',
      'settings' => array(
        'allowed_values' => array(
          'donors_enter_their_own_contribution_amount' => 'Donors enter their own contribution amount.',
          'donors_contribute_a_fixed_amount' => 'Donors contribute a fixed amount.',
        ),
        'allowed_values_function' => '',
      ),
    ),
    'donors_contribute_fixed_amount' => array(
      'field_name' => 'donors_contribute_fixed_amount',
      'label' => $label_title('Amount'),
      'type' => 'text',
    ),
    'donation_buttion_custom_image' => array(
      'field_name' => 'donation_buttion_custom_image',
      'label' => $label_title('Image'),
      'type' => 'image',
    ),
    'save_button_at_paypal' => array(
      'field_name' => 'save_button_at_paypal',
      'type' => 'list_text',
      'label' => $label_title('Save button at PayPal'),
      'description' => '<div class="info-list-wrapper">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <ul>
                                                            <li>Protect your buttons from fraudulent changes</li>
                                                            <li>Automatically add buttons to "My Saved Buttons" in your PayPal profile</li>
                                                            <li>Easily create similar buttons</li>
                                                            <li>Edit your buttons with PayPal\'s tools </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>',
      'cardinality' => '-1',
      'settings' => array(
        'allowed_values' => array(1 => "Save button at PayPal"),
        'allowed_values_function' => '',
      ),
    ),
    'add_customer_message' => array(
      'field_name' => 'add_customer_message',
      'type' => 'list_text',
      'label' => $label_title('Can your customer add special instructions in a message to you?'),
      'cardinality' => '1',
      'settings' => array(
        'allowed_values' => array(1 => 'Yes', 2 => 'No'),
        'allowed_values_function' => '',
      ),
    ),
    'add_customer_message_box' => array(
      'field_name' => 'add_customer_message_box',
      'type' => 'text',
      'label' => $label_title('Name of message box (40-character limit)'),
      'settings' => array(
        'max_length' => '40',
      ),
    ),
    'cancel_url_options' => array(
      'field_name' => 'cancel_url_options',
      'cardinality' => '-1',
      'type' => 'list_text',
      'label' => $label_title('Take customers to this URL when they cancel their checkout'),
      'settings' => array(
        'allowed_values' => array(1 => 'Yes'),
        'allowed_values_function' => '',
      ),
    ),
    'cancel_url' => array(
      'field_name' => 'cancel_url',
      'type' => 'text',
      'label' => $label_title('Cancel Url'),
    ),
    'finish_url_options' => array(
      'field_name' => 'finish_url_options',
      'cardinality' => '-1',
      'type' => 'list_text',
      'label' => $label_title('Take customers to this URL when they finish checkout'),
      'settings' => array(
        'allowed_values' => array(1 => 'Yes'),
        'allowed_values_function' => '',
      ),
    ),
    'finish_url' => array(
      'field_name' => 'finish_url',
      'type' => 'text',
      'label' => $label_title('Finish Url'),
    ),
    'donate_button_html' => array(
      'field_name' => 'donate_button_html',
      'label' => $label_title('Donate Button Html'),
      'type' => 'text_long',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
    ),
    'donate_button_email_link' => array(
      'field_name' => 'donate_button_email_link',
      'label' => $label_title('Donate Button Email Link'),
      'type' => 'text',
    ),
    'hosted_button_id' => array(
      'field_name' => 'hosted_button_id',
      'label' => $label_title('Hosted Button Id'),
      'type' => 'text',
    ),
  );

}

/**
 * Field instances.
 */
function _paypal_button_list_installed_instances() {
  $label_title = get_t();

  return array(
    'company_list' => array(
      'field_name' => 'company_list',
      'type' => 'options_select',
      'required' => 1,
      'label' => $label_title('Company List'),
      'widget' => array(
        'type' => 'options_select',
        'weight' => -4,
      ),
    ),
    'organization_name_service' => array(
      'field_name' => 'organization_name_service',
      'label' => $label_title('Organization name/service'),
      'widget' => array(
        'type' => 'text_textfield',
        'weight' => -3,
      ),
    ),
    'paypal_donation_id' => array(
      'field_name' => 'paypal_donation_id',
      'label' => $label_title('Donation ID'),
      'widget' => array(
        'type' => 'text_textfield',
        'weight' => -2,
      ),
    ),
    'customize_donation_button_type' => array(
      'field_name' => 'customize_donation_button_type',
      'label' => $label_title('Customize button'),
      'description' => '',
      'widget' => array(
        'type' => 'options_buttons',
      ),
      'default_value' => array(
        0 => array(
          'value' => 'paypal_button',
        ),
      ),
    ),
    'customize_donation_button' => array(
      'field_name' => 'customize_donation_button',
      'label' => $label_title('PayPal button'),
      'description' => '',
      'widget' => array(
        'type' => 'options_buttons',
              // 'weight' => -10,.
      ),

    ),
    'country_and_language_button' => array(
      'field_name' => 'country_and_language_button',
      'label' => $label_title('Country and language for button'),
      'description' => '',
      'widget' => array(
        'type' => 'options_select',
                // 'weight' => -10,.
      ),
      'default_value' => array(
        0 => array(
          'value' => 'en_AL',
        ),
      ),
    ),

    'select_currency' => array(
      'field_name' => 'select_currency',
      'label' => $label_title('Currency'),
      'description' => '',
      'widget' => array(
        'type' => 'options_select',
              // 'weight' => -10,.
      ),
      'default_value' => array(
        0 => array(
          'value' => 'USD',
        ),
      ),
    ),
    'select_contribution_amount' => array(
      'field_name' => 'select_contribution_amount',
      'label' => $label_title('Contribution amount'),
      'description' => '',
      'widget' => array(
        'type' => 'options_buttons',
         // 'weight' => -10,.
      ),
      'default_value' => array(
        0 => array(
          'value' => 'donors_enter_their_own_contribution_amount',
        ),
      ),
    ),

    'donors_contribute_fixed_amount' => array(
      'field_name' => 'donors_contribute_fixed_amount',
      'label' => $label_title('Amount'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'donation_buttion_custom_image' => array(
      'field_name' => 'donation_buttion_custom_image',
      'description' => 'Upload an image to go with this donation button.',
      'label' => $label_title('Image'),
      'widget' => array(
        'type' => 'image_image',
      ),
    ),
    'save_button_at_paypal' => array(
      'field_name' => 'save_button_at_paypal',
      'label' => $label_title('Save button at PayPal'),
      'description' => '<div class="info-list-wrapper">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <ul>
                                                            <li>Protect your buttons from fraudulent changes</li>
                                                            <li>Automatically add buttons to "My Saved Buttons" in your PayPal profile</li>
                                                            <li>Easily create similar buttons</li>
                                                            <li>Edit your buttons with PayPal\'s tools </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>',
      'widget' => array(
        'type' => 'options_buttons',
             // 'weight' => -10,.
      ),
      'default_value' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'add_customer_message' => array(
      'field_name' => 'add_customer_message',
      'label' => $label_title('Can your customer add special instructions in a message to you?'),
      'description' => '',
      'widget' => array(
        'type' => 'options_buttons',
                // 'weight' => -10,.
      ),
      'default_value' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'add_customer_message_box' => array(
      'field_name' => 'add_customer_message_box',
      'label' => $label_title('Name of message box (40-character limit)'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'cancel_url_options' => array(
      'field_name' => 'cancel_url_options',
      'label' => $label_title('Take customers to this URL when they cancel their checkout'),
      'widget' => array(
        'type' => 'options_buttons',
      ),
    ),
    'cancel_url' => array(
      'field_name' => 'cancel_url',
      'label' => $label_title('Cancel Url'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'finish_url_options' => array(
      'field_name' => 'finish_url_options',
      'label' => $label_title('Take customers to this URL when they finish checkout'),
      'widget' => array(
        'type' => 'options_buttons',
      ),
    ),
    'finish_url' => array(
      'field_name' => 'finish_url',
      'label' => $label_title('Finish Url'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'donate_button_html' => array(
      'field_name' => 'donate_button_html',
      'type' => 'text',
      'label' => $label_title('Donate Button Html'),
      'widget' => array(
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => 1,
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 1,
        ),
      ),
      'settings' => array(
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
    ),
    'donate_button_email_link' => array(
      'field_name' => 'donate_button_email_link',
      'label' => $label_title('Donate Button Email Link'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'hosted_button_id' => array(
      'field_name' => 'hosted_button_id',
      'label' => $label_title('Hosted Button Id'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),

  );
}

/**
 * Donate button node type create field and create instance functions call.
 */
function _paypal_button_list_add_fields() {

  foreach (_paypal_button_list_installed_fields() as $fields) {
    field_create_field($fields);
  }
  foreach (_paypal_button_list_installed_instances() as $fieldinstance) {
    $fieldinstance['entity_type'] = 'node';
    $fieldinstance['bundle'] = PAYPAL_DONATION_NODE_NAME;
    field_create_instance($fieldinstance);
  }

}

/**
 * Donate button node type delete field and create instance functions call.
 */
function _paypal_button_list_delete_fields() {
  foreach (array_keys(_paypal_button_list_installed_fields()) as $field) {
    field_delete_field($field);
  }
  $instances = field_info_instances('node', PAYPAL_DONATION_NODE_NAME);
  foreach ($instances as $instance_name => $fieldinstance) {
    _paypal_button_list_installed_instances($fieldinstance);
  }

}

/**
 * Donate button node type save field group function call.
 */
function _paypal_button_fields_group_save() {

  // Add fieldgroup:
  $donation_button_details_group_name = GROUP_DONATION_BUTTON_DETAILS;
  $customize_donation_button_details_name = GROUP_CUSTOMIZE_DONATION_BUTTON;
  $group_track_inventory_profit = GROUP_TRACK_INVENTORY_PROFIT;
  $group_advanced_features = GROUP_ADVANCED_FEATURES;
  $main_group = PARENT_GROUP;

  $entity_type = PAYPAL_DONATION_NODE_NAME;
  $bundle = "node";
  $mode = 'form';
  if (!field_group_exists($donation_button_details_group_name, $entity_type, $bundle, $mode)) {
    $customize_donation_button_details_name_group = (object) array(
      'identifier' => $customize_donation_button_details_name . '|' . $entity_type . '|' . $bundle . '|' . $mode,
      'group_name' => $customize_donation_button_details_name,
      'entity_type' => "node",
      'bundle' => PAYPAL_DONATION_NODE_NAME,
      'mode' => $mode,
      'label' => 'Customize button',
      'weight' => '1',
      'children' => array(
        'customize_donation_button_type',
        'customize_donation_button',
        'country_and_language_button',
        'donation_buttion_custom_image',

      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsed',
        'instance_settings' => array(
          'description' => '',
          'classes' => $customize_donation_button_details_name,
          'required_fields' => 1,
        ),
      ),
    );
    field_group_group_save($customize_donation_button_details_name_group);

    $donation_button_details_group_data = (object) array(
      'identifier' => $donation_button_details_group_name . '|' . $entity_type . '|' . $bundle . '|' . $mode,
      'group_name' => $donation_button_details_group_name,
      'entity_type' => "node",
      'bundle' => PAYPAL_DONATION_NODE_NAME,
      'mode' => $mode,
      'label' => 'Step 1: Choose a button type and enter your payment details',
      'weight' => '6',
      'children' => array(
        'organization_name_service',
        'paypal_donation_id',
        $customize_donation_button_details_name,
        'select_currency',
        'select_contribution_amount',
        'donors_contribute_fixed_amount',

      ),
      'format_type' => 'accordion-item',
      'format_settings' => array(
        'formatter' => 'closed',
        'instance_settings' => array(
          'description' => '',
          'classes' => $donation_button_details_group_name,
          'required_fields' => 1,
        ),
      ),
    );
    field_group_group_save($donation_button_details_group_data);

    $group_track_inventory_profit_data = (object) array(
      'identifier' => $group_track_inventory_profit . '|' . $entity_type . '|' . $bundle . '|' . $mode,
      'group_name' => $group_track_inventory_profit,
      'entity_type' => "node",
      'bundle' => PAYPAL_DONATION_NODE_NAME,
      'mode' => $mode,
      'label' => 'Step 2: Track inventory, profit & loss (optional)',
      'weight' => '7',
      'children' => array(
        'save_button_at_paypal',
      ),
      'format_type' => 'accordion-item',
      'format_settings' => array(
        'formatter' => 'closed',
        'instance_settings' => array(
          'description' => '',
          'classes' => $group_track_inventory_profit,
          'required_fields' => 1,
        ),
      ),
    );
    field_group_group_save($group_track_inventory_profit_data);

    $group_advanced_features_data = (object) array(
      'identifier' => $group_advanced_features . '|' . $entity_type . '|' . $bundle . '|' . $mode,
      'group_name' => $group_advanced_features,
      'entity_type' => "node",
      'bundle' => PAYPAL_DONATION_NODE_NAME,
      'mode' => $mode,
      'label' => 'Step 3: Customize advanced features (optional)',
      'weight' => '8',
      'children' => array(
        'add_customer_message',
        'add_customer_message_box',
        'finish_url',
        'finish_url_options',
        'cancel_url',
        'cancel_url_options',
      ),
      'format_type' => 'accordion-item',
      'format_settings' => array(
        'formatter' => 'closed',
        'instance_settings' => array(
          'description' => '',
          'classes' => $group_advanced_features,
          'required_fields' => 1,
        ),
      ),
    );
    field_group_group_save($group_advanced_features_data);

    $main_group_data = (object) array(
      'identifier' => $main_group . '|' . $entity_type . '|' . $bundle . '|' . $mode,
      'group_name' => $main_group,
      'entity_type' => "node",
      'bundle' => PAYPAL_DONATION_NODE_NAME,
      'mode' => $mode,
      'label' => 'Main Group',
      'weight' => '3',
      'children' => array(
        GROUP_DONATION_BUTTON_DETAILS,
        GROUP_TRACK_INVENTORY_PROFIT,
        GROUP_ADVANCED_FEATURES,
      ),
      'format_type' => 'accordion',
      'format_settings' => array(
        'formatter' => 'closed',
        'instance_settings' => array(
          'effect' => 'none',
          'classes' => 'group-main field-group-accordion',
          'id' => '',
        ),
      ),
    );
    field_group_group_save($main_group_data);
  }

}
