<?php

/**
 * @file
 * Donate Button company node field create and delete.
 */

// Machine name for our custom node.
define('PAYPAL_DONATION_COMPANY', "paypal_donation_companay");

/**
 * Filed installed.
 */
function _paypal_donation_companay_installed_fields() {
  $label_title = get_t();

  return array(
    'paypal_account_id' => array(
      'field_name' => 'paypal_account_id',
      'label' => $label_title('PayPal Account ID'),
      'type' => 'text',
    ),
    'company_name' => array(
      'field_name' => 'company_name',
      'label' => $label_title('Company Name'),
      'type' => 'text',
    ),
    'contact_name' => array(
      'field_name' => 'contact_name',
      'label' => $label_title('Contact Name'),
      'type' => 'text',
    ),
    'paypal_account_email' => array(
      'field_name' => 'paypal_account_email',
      'label' => $label_title('PayPal Account Email'),
      'type' => 'text',
    ),
    'api_username' => array(
      'field_name' => 'api_username',
      'label' => $label_title('API Username'),
      'type' => 'text',
    ),
    'api_password' => array(
      'field_name' => 'api_password',
      'label' => $label_title('API Password'),
      'type' => 'text',
    ),
    'api_signature' => array(
      'field_name' => 'api_signature',
      'label' => $label_title('API Signature'),
      'type' => 'text',
    ),
    'paypal_mode' => array(
      'field_name' => 'paypal_mode',
      'type' => 'list_text',
      'label' => $label_title('PayPal Mode'),
      'settings' => array(
        'allowed_values' => array(
          "Sandbox" => 'Sandbox',
          "Live" => 'Live',
        ),
        'allowed_values_function' => '',
      ),
    ),
    'button_account_id' => array(
      'field_name' => 'button_account_id',
      'type' => 'list_text',
      'label' => $label_title('Button Account ID'),
      'settings' => array(
        'allowed_values' => array(
          'paypal_account_id' => 'PayPal Account ID',
          'email_id' => 'Email Id',
        ),
        'allowed_values_function' => '',
      ),
    ),

  );

}

/**
 * Field instances.
 */
function _paypal_donation_companay_installed_instances() {
  $label_title = get_t();

  return array(
    'paypal_account_id' => array(
      'field_name' => 'paypal_account_id',
      'label' => $label_title('PayPal Account ID'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'company_name' => array(
      'field_name' => 'company_name',
      'label' => $label_title('Company Name'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'contact_name' => array(
      'field_name' => 'contact_name',
      'label' => $label_title('Contact Name'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'paypal_account_email' => array(
      'field_name' => 'paypal_account_email',
      'label' => $label_title('PayPal Account Email'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'api_username' => array(
      'field_name' => 'api_username',
      'label' => $label_title('API Username'),
      'required' => 1,
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'api_password' => array(
      'field_name' => 'api_password',
      'label' => $label_title('API Password'),
      'required' => 1,
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'api_signature' => array(
      'field_name' => 'api_signature',
      'label' => $label_title('API Signature'),
      'required' => 1,
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'paypal_mode' => array(
      'field_name' => 'paypal_mode',
      'type' => 'list_text',
      'required' => 1,
      'label' => $label_title('PayPal Mode'),
      'widget' => array(
        'type' => 'options_buttons',
         // 'weight' => -10,.
      ),
      'default_value' => array(
        0 => array(
          'value' => 'Sandbox',
        ),
      ),
    ),
    'button_account_id' => array(
      'field_name' => 'button_account_id',
      'type' => 'list_text',
      'label' => $label_title('Button Account ID'),
      'widget' => array(
        'type' => 'options_buttons',
         // 'weight' => -10,.
      ),
    ),

  );
}

/**
 * Donate button company create field and field instance function call.
 */
function _paypal_donation_companay_add_fields() {

  foreach (_paypal_donation_companay_installed_fields() as $fields) {
    field_create_field($fields);
  }
  foreach (_paypal_donation_companay_installed_instances() as $fieldinstance) {
    $fieldinstance['entity_type'] = 'node';
    $fieldinstance['bundle'] = PAYPAL_DONATION_COMPANY;
    field_create_instance($fieldinstance);
  }

}

/**
 * Donate button company delete field and field instance function call.
 */
function _paypal_donation_companay_delete_fields() {
  foreach (array_keys(_paypal_donation_companay_installed_fields()) as $field) {
    field_delete_field($field);
  }
  $instances = field_info_instances('node', PAYPAL_DONATION_COMPANY);
  foreach ($instances as $instance_name => $fieldinstance) {
    _paypal_donation_companay_installed_instances($fieldinstance);
  }

}
