<?php

/**
 * @file
 * Admin configure form control on page.
 */

/**
 * Admin simple donate button form.
 */
function paypal_simple_donate_button_details() {

  $form['paypal_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('PayPal Account ID'),
    '#default_value' => variable_get('paypal_account_id', ''),
    '#attributes' => array('readonly' => 'readonly'),
  );
  $form['company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#default_value' => variable_get('company_name', ''),

  );
  $form['contact_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Name'),
    '#default_value' => variable_get('contact_name', ''),

  );
  $form['paypal_account_email'] = array(
    '#type' => 'textfield',
    '#title' => t('PayPal Account Email'),
    '#required' => TRUE,
    '#default_value' => variable_get('paypal_account_email', ''),
  );
  $form['api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API Username'),
    '#required' => TRUE,
    '#default_value' => variable_get('api_username', ''),
    '#maxlength' => 250,
  );
  $form['api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API Password'),
    '#required' => TRUE,
    '#default_value' => variable_get('api_password', ''),
    '#maxlength' => 250,
  );
  $form['api_signature'] = array(
    '#type' => 'textfield',
    '#title' => t('API Signature'),
    '#required' => TRUE,
    '#default_value' => variable_get('api_signature', ''),
    '#maxlength' => 250,
  );
  $form['paypal_mode'] = array(
    '#type' => 'radios',
    '#title' => t('PayPal Mode'),
    '#required' => TRUE,
    '#options' => array('Sandbox' => t('Sandbox'), 'Live' => t('Live')),
    '#default_value' => variable_get('paypal_mode', ''),
  );
  $form['button_account_id'] = array(
    '#type' => 'radios',
    '#title' => t('Button Account ID'),
    '#required' => TRUE,
    '#options' => array('paypal_account_id' => t('PayPal Account ID'), 'email_id' => t('Email Address')),
    '#default_value' => variable_get('button_account_id', ''),
  );
  $form['paypal_donation_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Paypal Donation Amount'),
    '#default_value' => variable_get('paypal_donation_amount', ''),
  );
  return system_settings_form($form);

}
