<?php
/**
 * File simple donate button form
 */
if (!empty(variable_get('paypal_mode')) && !empty(variable_get('api_username')) && !empty(variable_get('api_password')) && !empty(variable_get('api_signature'))) {
    module_load_include("inc", "paypal_button_manager", 'includes/simple_donate_create_button');
    echo get_simple_donate_button();
} else {
    echo '<div id="messages"><div class="section clearfix">
      <div class="messages error">
<h2 class="element-invisible">Error message</h2>
<em class="placeholder">Error</em> PayPal API Credentials are Incorrect.</div>
    </div></div>';
}
