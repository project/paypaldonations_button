jQuery(document).ready(function() {

    //select paypal donation button design type
    jQuery(document).on("change",'input[type=radio][name="customize_donation_button_type[und]"]',function(){
        if (this.value == 'paypal_button'){
              jQuery(".field-name-donation-buttion-custom-image").hide();
              jQuery(".field-name-customize-donation-button").show();
              jQuery(".field-name-country-and-language-button").show();

          }else if (this.value == 'use_your_own_button_image') {
              jQuery(".field-name-customize-donation-button").hide();
              jQuery(".field-name-country-and-language-button").hide();
              jQuery(".field-name-donation-buttion-custom-image").show();

          }
    });

    //select amout fixed or not
    jQuery(document).on("change",'input[type=radio][name="select_contribution_amount[und]"]',function(){
        if (this.value == 'donors_enter_their_own_contribution_amount'){
              jQuery(".field-name-donors-contribute-fixed-amount").hide();
          }else if (this.value == 'donors_contribute_a_fixed_amount') {
              jQuery(".field-name-donors-contribute-fixed-amount").show();


          }
    });
     //select message display or not
    jQuery(document).on("change",'input[type=radio][name="add_customer_message[und]"]',function(){
        if (this.value == '1'){
              jQuery(".field-name-add-customer-message-box").show();
          }else if (this.value == '2') {
              jQuery(".field-name-add-customer-message-box").hide();


          }
    });
     //select message display or not
    jQuery(document).on("change",'input[type=radio][name="add_customer_message[und]"]',function(){
        if (this.value == '1'){
              jQuery(".field-name-add-customer-message-box").show();
          }else if (this.value == '2') {
              jQuery(".field-name-add-customer-message-box").hide();


          }
    });


    // add cancel url option
    jQuery(document).on('click','input[type=checkbox][name="cancel_url_options[und][1]"]',function(){
        if(jQuery(this).is(':checked')) {
                jQuery('.field-name-cancel-url .text-full').removeAttr("disabled");
                jQuery('.field-name-cancel-url .text-full').focus();
        } else {
              jQuery('.field-name-cancel-url .text-full').attr("disabled", "disabled");
            }
      });

     // add finish url option
    jQuery(document).on('click','input[type=checkbox][name="finish_url_options[und][1]"]',function(){
        if(jQuery(this).is(':checked')) {
                jQuery('.field-name-finish-url .text-full').removeAttr("disabled");
                jQuery('.field-name-finish-url .text-full').focus();
        } else {
              jQuery('.field-name-finish-url .text-full').attr("disabled", "disabled");
            }
      });


  });

